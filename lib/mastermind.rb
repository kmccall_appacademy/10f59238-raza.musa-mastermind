class Code
  attr_reader :pegs
  PEGS = {
    "R" => :red,
    "B" => :blue,
    "Y" => :yellow,
    "G" => :green,
    "P" => :purple,
    "O" => :orange
  }

  def self.parse(input)
    colors = input.chars.map do |letter|
      raise "error" unless PEGS.key?(letter.upcase)
      PEGS[letter.upcase]
    end
    Code.new(colors)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    Code.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(other_code)
    count = 0
    pegs.each_index do |idx|
      count += 1 if pegs[idx] == other_code[idx]
    end
    count
  end

  def near_matches(other_code)
    count = 0
    PEGS.values.each do |color|
      count += [@pegs.count(color), other_code.pegs.count(color)].min
    end
    count - exact_matches(other_code)
  end

  def==(other_code)
  return false unless other_code.is_a?(Code)
  self.pegs == other_code.pegs
  end

  def to_s
  "#{@pegs.join(", ")}"
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    guess_counter = 9
    count = 1
    until guess_counter == 0
      guess = get_guess
      if won?(guess)
        conclude(guess)
        puts "You took #{count} guesses to win!"
        display_matches(guess)
        return
      else
        puts "Guess gain! You have #{guess_counter} left"
        display_matches(guess)
      end
      guess_counter -= 1
      count += 1
    end
    conclude
  end


  def won?(guess)
    @secret_code == guess
  end

  def conclude(guess)
    if @secret_code == guess
      puts "Congratulations, you won!"
    else
      puts "Sorry, you lost :("
    end
    "The code was #{@secret_code}"
  end

  def get_guess
    puts "Enter a guess (e.g. BRBR)"
    answer = gets.chomp
    guess = Code.parse(answer)
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)

    puts "You got #{exact_matches} exact matches!"
    puts "You got #{near_matches} near matches!"
  end



end

if __FILE__ == $PROGRAM_NAME
  game = Game.new(Code.parse("brbr"))
  game.play
end
